var state = 0;
var internet = 0;
var maps = 0;

$(window).ready(function() {
	
	$("#navPanel").fadeIn();
	$("#slides").fadeIn();
	$("webview").css("width","100%");

	const webview = document.querySelector('webview');
	
	webview.addEventListener('load-commit', function(e) {
		$('#url').val('');
    });

	webview.addEventListener('did-finish-load', function(e) {
		$('#url').val(webview.getURL());
    });

	//Increment the idle time counter every minute.
    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
    });

    $(this).keypress(function (e) {
        idleTime = 0;
    });

});

function timerIncrement() {
    idleTime = idleTime + 1;
    if (idleTime > 14) { //15 Minute timeout
    	if(state == 1)
    	{
        	resetView();	
    	}
    }
}

function resetView()
{
	const webview = document.querySelector('webview');
	state = 0;
	internet = 0;
	maps = 0;
	things = 0;
	food = 0;
	
	$("#urlBar").hide();
	$("#buttonBar").hide();
	$("#overlay").hide();
	$(".title").show();

	$("webview").css("height","100%");

	$("#slidesParent").animate({
		width: '75%'
	},250);
	
	$("#navPanelParent").animate({
		width: '25%'
	},250);
	webview.clearHistory();
	webview.loadURL('http://www.tfpl.org/services/tfvc');
}

$("#food").click(function() {

	state = 1;
	internet = 0;
	maps = 0;
	things = 0;

	if(food == 0)
	{
		const webview = document.querySelector('webview');
		$("#urlBar").slideUp();

		webview.clearHistory();
		webview.loadURL("https://www.google.com/maps/search/restaurants/@42.5558999,-114.47629,13z/data=!3m1!4b1");
		
		$("webview").css("height","90%");
		$(".title").fadeOut(100);
		$("#buttonBar").slideDown();

		$("#slidesParent").animate({
			width: '94%'
		},500);
		
		$("#navPanelParent").animate({
			width: '6%'
		},500);	
		food = 1;
	}
});

$("#things").click(function() {

	state = 1;
	internet = 0;
	maps = 0;
	food = 0;

	if(things == 0)
	{
		const webview = document.querySelector('webview');
		$("#urlBar").slideUp();

		webview.clearHistory();
		webview.loadURL("https://www.google.com/maps/search/activities/@42.5558999,-114.47629,13z/data=!3m1!4b1");
		
		$("webview").css("height","90%");
		$(".title").fadeOut(100);
		$("#buttonBar").slideDown();

		$("#slidesParent").animate({
			width: '94%'
		},500);
		
		$("#navPanelParent").animate({
			width: '6%'
		},500);	
		things = 1;
	}
});

$("#maps").click(function() {

	state = 1;
	internet = 0;
	food = 0;
	things = 0;

	if(maps == 0)
	{
		const webview = document.querySelector('webview');
		$("#urlBar").slideUp();

		webview.clearHistory();
		webview.loadURL("http://www.google.com/maps");
		
		$("webview").css("height","90%");
		$(".title").fadeOut(100);
		$("#buttonBar").slideDown();

		$("#slidesParent").animate({
			width: '94%'
		},500);
		
		$("#navPanelParent").animate({
			width: '6%'
		},500);	
		maps = 1;
	}
});

$("#internet").click(function() {

	state = 1;
	maps = 0;
	food = 0;
	things = 0;

	if(internet == 0)
	{
		const webview = document.querySelector('webview');

		$(".title").fadeOut(100);

		$("#buttonBar").slideUp();
		$("#urlBar").slideDown();

		$("#slidesParent").animate({
			width: '94%'
		},500);
		
		$("#navPanelParent").animate({
			width: '6%'
		},500);

		$("webview").css("height","90%");

		webview.clearHistory();
		webview.loadURL("http://www.google.com");

		internet = 1;
	}
});


$("#refresh").click(function() {
	const webview = document.querySelector('webview');
	webview.reload();
});

$("#home").click(function() {
	const webview = document.querySelector('webview');
	webview.loadURL('http://www.google.com/');
});

$("#go").click(function() {
	const webview = document.querySelector('webview');
	url = $("#url").val();
	
	if(url.length == 0)
	{
		alert('Please provide an address.');
	}
	else
	{
		if(url.substr(0,7) != 'http://'){
	    url = 'http://' + url;
		}

		if(url.substr(url.length-1, 1) != '/'){
		    url = url + '/';
		}

		webview.loadURL(url);	

		$("#url").val(webview.getURL());

		if(webview.canGoForward())
		{
			$("#forward").enable();
		}
		else
		{
			$("#forward").disable();	
		}

		if(webview.canGoBack())
		{
			$("#back").enable();
		}
		else
		{
			$("#back").disable();	
		}
	}
});

$("#back").click(function() {
	const webview = document.querySelector('webview');
	webview.goBack();
});

$("#forward").click(function() {
	const webview = document.querySelector('webview');
	webview.goForward();
});

$(".end").click(function() {
	$("#overlay").fadeIn();
});

$(".goBack").click(function() {
	resetView();
});

$("#cancelClose").click(function() {
	$("#overlay").fadeOut();
});

$("#confirmClose").click(function() {
	const remote = require('electron').remote;
	var window = remote.getCurrentWindow();
    window.close();
});

$("#navPanel").mouseenter(function() {
	
	if(state == 1)
	{
		$("#url").css("width","40%");

		$("#slidesParent").animate({
			width: '75%'
		},250);
		
		$("#navPanelParent").animate({
			width: '25%'
		},250);

		setTimeout(function() {
			$(".title").fadeIn('fast');	
		},250);
	}
});

$("#navPanel").mouseleave(function() {
	
	if(state == 1)
	{
		$("#url").css("width","70%");

		$("#slidesParent").animate({
			width: '94%'
		},250);
		
		$("#navPanelParent").animate({
			width: '6%'
		},250);

		
		$(".title").hide();
		

		
	}

});